import axios from "axios";
import React, { useEffect, useState } from "react";
import { FaPlay, FaHeart } from "react-icons/fa";

const MovieRow = ({ title, url }) => {
  const [movies, setMovies] = useState([]);
  const [hoveredMovie, setHoveredMovie] = useState(null);

  useEffect(() => {
    axios.get(url).then((response) => setMovies(response.data.results));
  }, [url]);

  const playMovie = (movie) => {
    // Replace this with your actual logic to play the movie
    console.log(`Playing movie: ${movie.title}`);
  };

  const likeMovie = (movie) => {
    // Replace this with your actual logic to handle liking the movie
    console.log(`Liked movie: ${movie.title}`);
    movie.isLiked = !movie.isLiked;
    setHoveredMovie(movie);
  };

  return (
    <div className="mb-8">
      <h2
        className="font-nsans-bold text-xl p-4 capitalize cursor-pointer transition-transform transform hover:scale-105"
        onMouseEnter={() => setHoveredMovie(true)}
        onMouseLeave={() => setHoveredMovie(null)}>
        {title}
      </h2>

      <div className="relative flex items-center overflow-hidden">
        <div
          id={`slider`}
          className="w-full h-full overflow-x-scroll whitespace-nowrap scrollbar-hide custom-scrollbar">
          <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-4">
            {movies.map((movie) => (
              <div
                key={movie.id}
                className="relative group overflow-hidden"
                onMouseEnter={() => setHoveredMovie(movie)}
                onMouseLeave={() => setHoveredMovie(null)}>
                <img
                  className="w-full h-full object-cover object-top transition-transform transform hover:scale-105"
                  src={createImageUrl(movie.backdrop_path, "original")}
                  alt={movie.title}
                />
                {hoveredMovie === movie && (
                  <div className="absolute inset-0 flex flex-col items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity cursor-pointer">
                    <div className="bg-black bg-opacity-50 p-2 rounded-full mb-2 flex items-center justify-center">
                      <FaPlay
                        className="w-6 h-6 text-white mr-2 cursor-pointer"
                        onClick={() => playMovie(movie)}
                      />
                    </div>
                    <div className="absolute inset-x-0 bottom-0 p-2 bg-black bg-opacity-50 text-white transition-opacity">
                      <p className="text-sm">{movie.title}</p>
                    </div>
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

const createImageUrl = (filename, size) => {
  return `https://image.tmdb.org/t/p/${size}/${filename}`;
};

export default MovieRow;
