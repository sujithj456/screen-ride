import React, { useState } from "react";
import { Link } from "react-router-dom";

const Login = () => {
  const [rememberLogin, setRememberLogin] = useState(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();
    console.log("Email:", email);
    console.log("Password:", password);
    // Add authentication logic here
  };

  return (
    <div className="relative w-full h-screen">
      {/* Background image and overlay */}
      <img
        className="hidden sm:block absolute w-full h-full object-cover"
        src="https://thefatork.store/cdn/shop/articles/netflix.jpg?v=1669877376"
        alt="Background"
      />
      <div className="bg-black/70 fixed top-0 left-0 w-full h-screen" />

      <div className="fixed w-full px-4 py-24 z-20">
        {/* Login form container */}
        <div className="max-w-[450px] h-[600px] mx-auto bg-black/80 rounded-lg">
          <div className="max-w-[320px] mx-auto py-16">
            {/* Login header */}
            <h1 className="text-3xl font-nsans-bold">Login</h1>

            {/* Login form */}
            <form
              onSubmit={handleFormSubmit}
              className="w-full flex flex-col py-4">
              {/* Email input */}
              <input
                className="p-3 my-2 bg-gray-700 rounded"
                type="email"
                placeholder="Email"
                autoComplete="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />

              {/* Password input */}
              <input
                className="p-3 my-2 bg-gray-700 rounded"
                type="password"
                placeholder="Password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              {/* Login button */}
              <button className="bg-red-600 py-3 my-6 rounded font-nsans-bold">
                Login
              </button>

              {/* Remember me checkbox and help links */}
              <div className="flex justify-between items-center text-gray-600">
                <p>
                  <input
                    type="checkbox"
                    className="mr-2"
                    checked={rememberLogin}
                    onChange={() => setRememberLogin(!rememberLogin)}
                  />
                  Remember me
                </p>
                <p>Need Help?</p>
              </div>

              {/* Sign-up link */}
              <p className="my-4">
                <span className="text-gray-600 mr-2">Not a member yet?</span>
                <Link to="/signup">Sign Up</Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
